package com.pms.demo.controller;

import java.util.List;
import java.util.Random;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pms.demo.Entity.Customer;
import com.pms.demo.Entity.Product;
import com.pms.demo.Entity.order;
import com.pms.demo.Service.CustomerService;
import com.pms.demo.Service.Email;
import com.pms.demo.Service.OrderService;
import com.pms.demo.Service.ProductService;
import com.pms.demo.dto.OrderDTO;
import com.pms.demo.dto.ResponseOrderDTO;
import com.pms.demo.util.DateUtil;

import ch.qos.logback.classic.Logger;

@RestController
@RequestMapping("/")
public class ShoppingCartRestController {

	@Autowired

	@Qualifier("email")

	private Email senderService;

	private OrderService orderService;
	private ProductService productService;
	private CustomerService customerService;

	public ShoppingCartRestController(OrderService orderService, ProductService productService,
			CustomerService customerService) {
		this.orderService = orderService;
		this.productService = productService;
		this.customerService = customerService;
	}

	private org.slf4j.Logger logger = LoggerFactory.getLogger(ShoppingCartRestController.class);

	@RequestMapping("sts")
	@ResponseBody
	public String Sts(ResponseOrderDTO responseOrderDTO) {
		String s = String.valueOf(responseOrderDTO);
		senderService.sendEmail("subramani832ak@gmail.com", "Purchased History", s);

		return "mail sended";
	}

	@GetMapping(value = "/getAllProducts")
	public ResponseEntity<List<Product>> getAllProducts() {

		List<Product> productList = productService.getAllProducts();

		return ResponseEntity.ok(productList);
	}

	@GetMapping(value = "/getOrder/{orderId}")
	public ResponseEntity<order> getOrderDetails(@PathVariable int orderId) {

		order order = orderService.getOrderDetail(orderId);
		return ResponseEntity.ok(order);
	}

	@PostMapping("/placeOrder")
	public ResponseEntity<ResponseOrderDTO> placeOrder(@RequestBody OrderDTO orderDTO) {
		logger.info("Request Payload " + orderDTO.toString());
		ResponseOrderDTO responseOrderDTO = new ResponseOrderDTO();
		float amount = orderService.getCartAmount(orderDTO.getCartItems());

		Customer customer = new Customer(orderDTO.getCustomerName(), orderDTO.getCustomerEmail());
		Integer customerIdFromDb = customerService.isCustomerPresent(customer);
		if (customerIdFromDb != null) {
			customer.setId(customerIdFromDb);
			logger.info("Customer already present in db with id : " + customerIdFromDb);
		} else {
			customer = customerService.saveCustomer(customer);
			logger.info("Customer saved.. with id : " + customer.getId());
		}

		order order = new order(orderDTO.getOrderDescription(), customer, orderDTO.getCartItems());

		order = orderService.saveOrder(order);
		logger.info("Order processed successfully..");

		responseOrderDTO.setAmount(amount);
		responseOrderDTO.setDate(DateUtil.getCurrentDateTime());
		responseOrderDTO.setInvoiceNumber(new Random().nextInt(1000));
		responseOrderDTO.setOrderId(order.getId());
		responseOrderDTO.setOrderDescription(orderDTO.getOrderDescription());

		logger.info("test push..");

		return ResponseEntity.ok(responseOrderDTO);
	}

}
