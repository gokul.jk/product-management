package com.shopping_list.shopping_list.entity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="product")

public class Shopping {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int Id;
	
	@Column(name="Name")
	private String name;
	
	@Column(name="Description")
	private String description;
	
	
	//constructor
	public Shopping() {}
	
	public Shopping(String name, String description) {
		this.name = name;
		this.description = description;
	
	}

	
	
	
	//getter and setter
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	
	//toString
//	@Override
//	public String toString() {
//		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
//	}

	@Override
	public String toString() {
		return "Shopping [Id=" + Id + ", name=" + name + ", description=" + description + "]";
	}

	
}


