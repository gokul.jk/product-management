package com.productmanagement.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Id;

@Entity
@Table(name="product")

public class ShoppingDAO {

@Id
@Column(name="id")
private int Id;
@Column(name="name")
private String name;
@Column(name="description")
private String description;

public ShoppingDAO() {
	super();
	// TODO Auto-generated constructor stub
}

public ShoppingDAO(int id, String name, String description) {
	super();
	Id = id;
	this.name = name;
	this.description = description;
}

public int getId() {
	return Id;
}

public void setId(int id) {
	Id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}



}
