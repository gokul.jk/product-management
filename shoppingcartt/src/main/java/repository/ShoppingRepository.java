package repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import entity.ShoppingCart;


@Repository
public interface ShoppingRepository extends JpaRepository<ShoppingCart,Integer> {

}
