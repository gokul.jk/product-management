package com.shopping_list.shopping_list.dao;

import java.util.List;

import com.shopping_list.shopping_list.entity.Shopping;

public interface ShoppingDAO {

	void delete(int studentId);

	void save(Shopping theShopping);

	List<Shopping> findAll();

	Shopping findById(int Id);

	void update(Shopping theShopping);

}
