package com.shopping_list.shopping_list.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shopping_list.shopping_list.entity.Shopping;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

@Repository

public class ShoppingDAOImpl implements ShoppingDAO{

	@Autowired
	private EntityManager entityManager;

	@Override
	@Transactional
	public void save(Shopping theShopping) {
		entityManager.persist(theShopping);
		
	}

	@Override
	public Shopping findById(int id) {
		// TODO Auto-generated method stub
		
		return entityManager.find(Shopping.class,id);
	}

	@Override
	public List<Shopping> findAll() {
		TypedQuery<Shopping> theQuery=entityManager.createQuery("From Shopping",Shopping.class);
		return theQuery.getResultList();
	}

	@Override
	@Transactional
	public void update(Shopping theShopping) {
		
		entityManager.merge(theShopping);
		
	}

	@Override
	@Transactional
	public void delete(int Id) {
		Shopping theShopping=entityManager.find(Shopping.class,Id);
		entityManager.remove(theShopping);
		
	}

	
}
