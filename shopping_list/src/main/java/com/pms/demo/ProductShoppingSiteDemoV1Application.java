package com.pms.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductShoppingSiteDemoV1Application {

	public static void main(String[] args) {
		SpringApplication.run(ProductShoppingSiteDemoV1Application.class, args);
	}

}
