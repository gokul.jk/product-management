package com.productmanagement.repo;
import org.springframework.data.jpa.repository.JpaRepository;

import com.productmanagement.entity.ShoppingDAO;;

public interface ShoppingInterface extends JpaRepository<ShoppingDAO, Integer> {

}
