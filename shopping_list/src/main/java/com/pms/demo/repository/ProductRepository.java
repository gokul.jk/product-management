package com.pms.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pms.demo.Entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {

	
}