package com.productmanagement.dao;


	import java.util.List;
	import java.util.Optional;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Controller;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.ResponseBody;
	import com.productmanagement.entity.ShoppingDAO;
    import com.productmanagement.repo.ShoppingInterface;

	@Controller
	
	public class ShoppingController {
		@Autowired
		ShoppingInterface rep;
		
		@RequestMapping("/")
		public String home() {
			
			return "index.jsp";
		}
		
		@RequestMapping("/addproduct")
		@ResponseBody
		public String addAlien(ShoppingDAO Product) {
			rep.save(Product);
			return "Data entered";
		}
		
		
		
		@RequestMapping("/getproduct")
		@ResponseBody
	     public Optional<ShoppingDAO> getAliens( int id) {
			System.out.println("success");
			return rep.findById(id);
		}
		
		@RequestMapping("/getproducts")
		@ResponseBody
	     public List<ShoppingDAO> getAliens() {
			return (List<ShoppingDAO>) rep.findAll();
		}
		
	 
	}


