package com.shopping_list.shopping_list;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.shopping_list.shopping_list.dao.ShoppingDAO;
import com.shopping_list.shopping_list.entity.Shopping;

@SpringBootApplication
public class ShoppingListApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingListApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner commandlinerunner(ShoppingDAO shoppingDAO) {
		return runner ->{
		   createshopping(shoppingDAO);
//		   createmultipleshopping(shoppingDAO);
//		   Findshopping(shoppingDAO);
//		   FindAllshopping(shoppingDAO);
//		   updateshopping(shoppingDAO);
//		   deleteshopping(shoppingDAO);
	};
	}

	private void deleteshopping(ShoppingDAO shoppingDAO) {
		int shoppingId=1;
		shoppingDAO.delete(shoppingId);
		
	}

	private void updatestudent(ShoppingDAO shoppingDAO) {
		
		int shoppingId=2;
		Shopping myShopping=shoppingDAO.findById(shoppingId);
		myShopping.setName("Scooby");
		System.out.println(myShopping);
		
	}

	private void FindAllshopping(ShoppingDAO shoppingDAO) {
		List<Shopping> theShopping=shoppingDAO.findAll();
		for(Shopping tempShopping:theShopping) {
			System.out.println(tempShopping);
		}
		
	}

	private void Findshopping(ShoppingDAO shoppingDAO) {
		int shoppingId=3;
		Shopping myShopping=shoppingDAO.findById(shoppingId);
		System.out.println(myShopping);
		
	}

	private void createmultipleshopping(ShoppingDAO shoppingDAO) {
		// TODO Auto-generated method stub
		Shopping tempShopping1=new Shopping("g","g");
		Shopping tempShopping2=new Shopping("s","p");
		Shopping tempShopping3=new Shopping("A","p");
		
		shoppingDAO.save(tempShopping1);
		shoppingDAO.save(tempShopping2);
		shoppingDAO.save(tempShopping3);

		
		

	}

	private void createshopping(ShoppingDAO shoppingDAO){
		System.out.println("ji");
		Shopping tempShopping=new Shopping("Oneplus","8 GB RAM & 128 GB Storage ");
		System.out.println("ji");
		shoppingDAO.save(tempShopping);
         
}
}